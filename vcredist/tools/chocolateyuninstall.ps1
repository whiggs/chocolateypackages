


$ErrorActionPreference = 'Stop'
$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  softwareName  = 'vcredist*'
  fileType      = 'EXE'
  silentArgs   = '/aiR'
  validExitCodes= @(0)
}
$arch = Get-OSArchitectureWidth
If ($arch -eq 32)
{
	$thename = "VisualCppRedist_AIO_x86only"
}
else
{
	$thename = "VisualCppRedist_AIO_x86_x64"
}

Uninstall-ChocolateyPackage @packageArgs

do
{
	Start-sleep 1
Until ((Get-process).name -notcontains $thename)



