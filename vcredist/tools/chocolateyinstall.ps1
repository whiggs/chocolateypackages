
$ErrorActionPreference = 'Stop'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://github.com/abbodi1406/vcredist/releases/download/v0.78.0/VisualCppRedist_AIO_x86only.exe'
$url64      = 'https://github.com/abbodi1406/vcredist/releases/download/v0.78.0/VisualCppRedist_AIO_x86_x64.exe'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64

  softwareName  = 'vcredist*'

  checksum      = 'F2452C9C6DCCF6EFF247C56525CBF0AEB8108AC293FD1B77ADABAC9066097BF6'
  checksumType  = 'sha256'
  checksum64    = '68AB06AE1D19045D1EA9EC87FE67C2102C8B09ACA2C7FF3DE897AEBE7FE80F11'
  checksumType64= 'sha256'

  silentArgs   = '/ai /gm2'
  validExitCodes= @(0)
}
$arch = Get-OSArchitectureWidth
If ($arch -eq 32)
{
	$thename = "VisualCppRedist_AIO_x86only"
}
else
{
	$thename = "VisualCppRedist_AIO_x86_x64"
}

Install-ChocolateyPackage @packageArgs

do
{
	Start-sleep 1
Until ((Get-process).name -notcontains $thename)















